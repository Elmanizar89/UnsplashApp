package com.unsplash.app.api

data class LoginResponse (
    val status: Boolean,
    val token: String?
)