package com.unsplash.app.api

import retrofit2.http.*

interface UnsplashApi {

    companion object {
        const val BASE_URL = "https://api.unsplash.com/"
        const val CLIENT_ID = "j9e2ReQHLPwTQUksDPFLM9mPyUmcpJ-iOe6ylcCXvnE"
    }

    @Headers("Accept-Version: v1", "Authorization: Client-ID $CLIENT_ID")
    @GET
    suspend fun searchPhotos(
        @Url url: String? = BASE_URL + "/search/photos",
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): UnsplashResponse

    @Headers("X-API-KEY: 454041184B0240FBA3AACD15A1F7A8BB")
    @FormUrlEncoded
    @POST
    suspend fun login(
        @Url url: String? = "",
        @Field("username") username: String,
        @Field("password") password: String
    ): LoginResponse
}