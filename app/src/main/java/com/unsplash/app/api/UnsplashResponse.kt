package com.unsplash.app.api

import com.unsplash.app.data.UnsplashPhoto

data class UnsplashResponse(
    val results: List<UnsplashPhoto>
)