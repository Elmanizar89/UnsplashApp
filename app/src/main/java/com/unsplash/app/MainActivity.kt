package com.unsplash.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    var lastInteraction = Date()
    var timer = object : CountDownTimer(1 * 10 * 1000, 1000) {
        override fun onTick(p0: Long) {

        }

        override fun onFinish() {
            finish()
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_main) as NavHostFragment
        navController = navHostFragment.findNavController()

        val appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
        val sharedPreference = applicationContext?.getSharedPreferences("unsplash", Context.MODE_PRIVATE)
        sharedPreference?.edit()?.clear()?.commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        val sharedPreference = applicationContext?.getSharedPreferences("unsplash", Context.MODE_PRIVATE)
        val token = sharedPreference?.getString("token", "-") ?: "-"
        if(token != "-"){
            timer.cancel()
            timer.start()
        }
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        if(fragment.id == 0){
            timer.cancel()
            timer.start()
        }
    }
}