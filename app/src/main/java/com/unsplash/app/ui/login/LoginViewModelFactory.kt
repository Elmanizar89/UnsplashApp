package com.unsplash.app.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.unsplash.app.api.UnsplashApi
import com.unsplash.app.data.LoginDataSource
import com.unsplash.app.data.LoginRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory (private val unsplashApi: UnsplashApi): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource()
                ),
                unsplashApi = unsplashApi
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}