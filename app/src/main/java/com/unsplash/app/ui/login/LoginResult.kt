package com.unsplash.app.ui.login

import com.unsplash.app.api.LoginResponse

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
    val success: LoginResponse? = null,
    val error: Int? = null
)