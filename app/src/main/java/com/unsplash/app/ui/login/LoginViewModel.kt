package com.unsplash.app.ui.login

import android.util.Base64
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.paging.PagingSource
import com.unsplash.app.data.LoginRepository
import com.unsplash.app.data.Result

import com.unsplash.app.R
import com.unsplash.app.api.LoginResponse
import com.unsplash.app.api.UnsplashApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.HttpException
import java.io.IOException
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class LoginViewModel(private val loginRepository: LoginRepository, private val unsplashApi: UnsplashApi) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    fun login(username: String, password: String) {
        runBlocking {
            // can be launched in a separate asynchronous job
            var x: Boolean = false
            var loginResponse: LoginResponse? = null
            val job = GlobalScope.launch {
                try{
                    //            val response = unsplashApi.login("https://talentpool.oneindonesia.id/api/user/login", "demo@demo.com", "demo123")
                    val response = unsplashApi.login("https://talentpool.oneindonesia.id/api/user/login", username, password)
                    x = response.status
                    loginResponse = response
                } catch (exception: IOException) {
                    x = false
                } catch (exception: HttpException) {
                    x = false
                }
            }
            job.join()
            if(x){
                _loginResult.value = LoginResult(success = loginResponse)
            }
            else{
                _loginResult.value = LoginResult(error = R.string.login_failed)
            }
//        val result = loginRepository.login(username, password)
//
//        if (result is Result.Success) {
//            _loginResult.value =
//                LoginResult(success = LoggedInUserView(displayName = result.data.displayName))
//        } else {
//            _loginResult.value = LoginResult(error = R.string.login_failed)
//        }
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains("@")) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}